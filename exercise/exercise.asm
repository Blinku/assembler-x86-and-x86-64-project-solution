STDIN equ 0
STDOUT equ 1
SYS_READ equ 0
SYS_WRITE equ 1

console_read:
    mov rax, SYS_READ
    mov rdi, STDIN
    mov rsi, buffer
    mov rdx, buffer_size
    
    syscall

console_write:
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    mov rsi, buffer
    mov rdx, buffer_size
    syscall   

    ret

to_upper:
    mov rsi, 0

l3:
    cmp rsi, buffer_size
    jg l1
    
    mov al, byte [buffer + rsi * 1] 
    cmp al, 'a' 
    jl l2
    
    cmp al, 'z'
    jg l2

    sub al, 32
    mov byte [buffer + rsi * 1], al

l2:
    inc rsi
    jmp l3

l1:
    ret
    

section	.data
buffer times 256 db 0x0
buffer_size equ $ - buffer

error_msg db " Error", 0x0
error_msg_size equ $ - error_msg

section	.text
global _start     
_start:
    call console_read
    
    cmp byte [buffer + buffer_size - 1], 0x0
    jne l4

    call to_upper
    call console_write

    jmp exit_progam

l4:
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    mov rsi, error_msg
    mov rdx, error_msg_size
    syscall

exit_progam:
    mov	eax, 60
    xor rdi, rdi
    syscall

