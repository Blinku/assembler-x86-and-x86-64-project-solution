FROM ubuntu:20.04

RUN apt-get update -y
RUN apt-get install -y binutils nasm vim

COPY exercise/test.sh /home/root/
COPY exercise/test_cases.txt /home/root/
COPY exercise/exercise.asm /home/root/

WORKDIR /home/root

RUN chmod +x test.sh
CMD ./test.sh