#!/bin/bash

if [ $# == 0 ];
then
  echo 'Podaj argument'
  exit 1
fi

if [ $1 == 'clone' ]
then

  echo '================================================';
  echo '=============Klonowanie repozytorium============';
  echo '================================================';
  echo

  git clone https://gitlab.com/Blinku/assembler-x86-and-x86-64-project-solution.git

elif [ $1 == 'run' ]
then

  if [[ -d "assembler-x86-and-x86-64-project-solution" ]]
  then
      cd assembler-x86-and-x86-64-project-solution
  fi

  echo '================================================';
  echo '==========Budowanie obrazu Dockerowego==========';
  echo '================================================';
  echo

  docker build -t gutlukasz-solution:1.0 .

  echo
  echo '================================================';
  echo '==============Odpalanie środowiska==============';
  echo '================================================';
  echo

  docker run gutlukasz-solution:1.0

elif [ $1 == 'clean' ]
then

  echo
  echo '================================================';
  echo '==========Usuwanie pobranej zawartości==========';
  echo '================================================';
  echo

  if [[ ! -d "assembler-x86-and-x86-64-project-solution" ]]
  then
      cd .. 
  fi
  
  rm -rf assembler-x86-and-x86-64-project-solution
  docker image rm -f gutlukasz-solution:1.0

elif [ $1 == 'solution' ]
then
  echo
  echo '================================================';
  echo '=============Pobieranie rozwiązania=============';
  echo '================================================';
  echo

  git clone https://gitlab.com/Blinku/assembler-x86-and-x86-64-project-solution.git
  cd assembler-x86-and-x86-64-project-solution
  ./run_gut_lukasz.sh run
fi

exit 0
